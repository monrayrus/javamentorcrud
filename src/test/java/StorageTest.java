import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.khusnullin.strings.Storage;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

class StorageTest {
    private Storage storage;

    @BeforeEach
    void setUp() {
        storage = new Storage();
    }

    @Test
    void testCreate() {
        int id = storage.create("value");
        assertEquals("value", storage.read(id));
    }

    @Test
    void testRead() {
        storage.create("value");
        int id = storage.create("value2");
        assertEquals("value2", storage.read(id));
    }

    @Test
    void testReadWithInvalidId() {
        assertThrows(IllegalArgumentException.class, () -> storage.read(123));
    }

    @Test
    void testUpdate() {
        int id = storage.create("value");
        storage.update(id, "new value");
        assertEquals("new value", storage.read(id));
    }

    @Test
    void testUpdateWithInvalidId() {
        assertThrows(IllegalArgumentException.class, () -> storage.update(123, "new value"));
    }

    @Test
    void testDelete() {
        int id = storage.create("value");
        storage.delete(id);
        assertThrows(IllegalArgumentException.class, () -> storage.read(id));
    }

    @Test
    void testDeleteWithInvalidId() {
        assertThrows(IllegalArgumentException.class, () -> storage.delete(123));
    }

    @Test
    void testGetAll() {
        storage.create("value1");
        storage.create("value2");
        storage.create("value3");
        Map<Integer, String> allStrings = storage.getAll();
        assertEquals(3, allStrings.size());
    }
}