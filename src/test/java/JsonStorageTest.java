import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.khusnullin.jsons.JsonStorage;
import ru.khusnullin.models.Person;

import java.io.File;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class JsonStorageTest {
    private JsonStorage jsonStorage;

    @BeforeEach
    public void setUp() {
        jsonStorage = new JsonStorage();
    }

    @AfterEach
    public void cleanUp() {
        File file = new File(jsonStorage.getFilePath());
        if (file.exists()) {
            file.delete();
        }
    }

    @Test
    public void createAndGetTest() {
        String personJson = "{\"name\":\"John Doe\",\"age\":30}";
        int id = jsonStorage.create(personJson);
        Person person = jsonStorage.read(id);
        assertEquals("John Doe", person.getName());
        assertEquals(30, person.getAge());
    }

    @Test
    public void updateTest() {
        String personJson = "{\"name\":\"John Doe\",\"age\":30}";
        int id = jsonStorage.create(personJson);
        String updatedPersonJson = "{\"name\":\"Jane Doe\",\"age\":25}";
        jsonStorage.update(id, updatedPersonJson);
        Person updatedPerson = jsonStorage.read(id);
        assertEquals("Jane Doe", updatedPerson.getName());
        assertEquals(25, updatedPerson.getAge());
    }

    @Test
    public void deleteTest() {
        String personJson = "{\"name\":\"John Doe\",\"age\":30}";
        int id = jsonStorage.create(personJson);
        jsonStorage.delete(id);
        assertThrows(IllegalArgumentException.class, () -> jsonStorage.read(id));
    }

    @Test
    public void getAllTest() {
        String person1Json = "{\"name\":\"John Doe\",\"age\":30}";
        int id1 = jsonStorage.create(person1Json);
        String person2Json = "{\"name\":\"Jane Doe\",\"age\":25}";
        int id2 = jsonStorage.create(person2Json);
        Map<Integer, Person> allValues = jsonStorage.getAll();
        assertEquals(2, allValues.size());
        assertEquals("John Doe", allValues.get(id1).getName());
        assertEquals(30, allValues.get(id1).getAge());
        assertEquals("Jane Doe", allValues.get(id2).getName());
        assertEquals(25, allValues.get(id2).getAge());
    }

    @Test
    public void getAllJsonObjectsTest() {
        String person1Json = "{\"name\":\"John Doe\",\"age\":30}";
        jsonStorage.create(person1Json);
        String person2Json = "{\"name\":\"Jane Doe\",\"age\":25}";
        jsonStorage.create(person2Json);
        List<JsonNode> allJsonObjects = jsonStorage.getAllJsonObjects();
    }
}