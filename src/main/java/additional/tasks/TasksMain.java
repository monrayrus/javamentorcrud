package additional.tasks;

import java.util.Arrays;

public class TasksMain {
    public static void main(String[] args) {
        System.out.println(DateParser.parseFromString("01.04.1992"));

        String csvExample =
                "1997,Ford,E350,\"ac, abs, moon\",3000.00\n" +
                "1999,Chevy,\"Venture \"\"Extended Edition\"\"\",\"\",4900.00\n" +
                "1996,Jeep,Grand Cherokee,\"MUST SELL! air, moon roof, loaded\",4799.00";
        String[][] csv = CSVParser.parseCsvToString(csvExample);
        for(int i = 0; i < csv.length; i++) {
            System.out.println(Arrays.toString(csv[i]));
        }


    }
}
