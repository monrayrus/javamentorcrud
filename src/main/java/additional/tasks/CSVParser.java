package additional.tasks;

import java.util.Arrays;
import java.util.Scanner;

public class CSVParser {
    public static String[][] parseCsvToString(String csv) {
        String[] lines = csv.split("\n");
        String[][] result = new String[lines.length][];
        for(int i = 0; i < lines.length; i++) {
            result[i] = lines[i].split(",");
        }
        return  result;
    }
}
