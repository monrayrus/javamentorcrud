package ru.khusnullin.jsons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.khusnullin.models.Person;

import java.io.*;
import java.util.*;

public class JsonStorage {
    private Map<Integer, Person> storage = new HashMap<>();
    private int idCounter = 0;

    public String getFilePath() {
        return filePath;
    }

    private String filePath;

    private final ObjectMapper objectMapper;

    public JsonStorage() {
        Properties prop = new Properties();
        InputStream input = null;
        objectMapper = new ObjectMapper();

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            input = classLoader.getResourceAsStream("config.properties");
            prop.load(input);
            filePath = prop.getProperty("filename");
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        loadFromDisk();
    }

    public int create(String value) {
        Objects.requireNonNull(value, "String value cannot be null");
        int id = ++idCounter;
        Person person = null;
        try {
            person = objectMapper.readValue(value, Person.class);
            System.out.println(person.getName() + ":" + person.getAge());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        storage.put(id, person);
        save();
        return id;
    }

    public Person read(int id) {
        Person value = storage.get(id);
        if (value == null) {
            throw new IllegalArgumentException("String with id " + id + " does not exist");
        }
        return value;
    }

    public void update(int id, String value) {
        Objects.requireNonNull(value, "String value cannot be null");
        if (!storage.containsKey(id)) {
            throw new IllegalArgumentException("String with id " + id + " does not exist");
        }
        Person person = null;
        try {
            person = objectMapper.readValue(value, Person.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        storage.put(id, person);
        save();
    }

    public void delete(int id) {
        if (!storage.containsKey(id)) {
            throw new IllegalArgumentException("String with id " + id + " does not exist");
        }
        storage.remove(id);
        save();
    }

    public Map<Integer, Person> getAll() {
        return new HashMap<>(storage);
    }

    public List<JsonNode> getAllJsonObjects() {
        List<JsonNode> jsonObjects = new ArrayList<>();
        for (Person person : storage.values()) {
            try {
                String jsonStr = objectMapper.writeValueAsString(person);
                JsonNode jsonNode = objectMapper.readTree(jsonStr);
                jsonObjects.add(jsonNode);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return jsonObjects;
    }

    private void loadFromDisk() {
        File file = new File(filePath);
        if(!file.exists()) {
            return;
        }
        int id = 0;
        try(BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while((line = reader.readLine()) != null) {
                JsonNode jsonNode = objectMapper.readTree(line);
                Person person = objectMapper.readValue(jsonNode.toString(), Person.class);
                storage.put(++id, person);
                idCounter = Math.max(idCounter, id);
                }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void save( ) {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for(Map.Entry<Integer, Person> entry : storage.entrySet()) {
                writer.write(objectMapper.writeValueAsString(entry.getValue()));
                writer.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
