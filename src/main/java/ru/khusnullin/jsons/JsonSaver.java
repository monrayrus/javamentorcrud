package ru.khusnullin.jsons;

import ru.khusnullin.strings.Storage;

import java.util.Scanner;

public class JsonSaver {
    private static final String CREATE_COMMAND = "CREATE";
    private static final String GET_COMMAND = "GET";
    private static final String UPDATE_COMMAND = "UPDATE";
    private static final String DELETE_COMMAND = "DELETE";
    private static final String QUIT_COMMAND = "QUIT";


    private static final JsonStorage storage = new JsonStorage();
    private static Scanner scanner = new Scanner(System.in);

    public static void startApp(){
        while (true) {
            String input = scanner.nextLine();
            if (input.startsWith(CREATE_COMMAND)) {
                String value = input.substring(CREATE_COMMAND.length() + 1);
                try {
                    int id = storage.create(value);
                    System.out.println("String saved with id = " + id);
                } catch (IllegalArgumentException e) {
                    System.err.println(e);
                }
            } else if (input.startsWith(GET_COMMAND)) {
                if (input.length() == GET_COMMAND.length()) {
                    System.out.println(storage.getAllJsonObjects());
                } else {
                    try {
                        int id = Integer.parseInt(input.substring(GET_COMMAND.length() + 1));
                        System.out.println(storage.read(id));
                    } catch (IllegalArgumentException e) {
                        System.err.println(e);
                    }
                }
            } else if (input.startsWith(UPDATE_COMMAND)) {
                String[] parts = input.split(" ", 3);
                int id = Integer.parseInt(parts[1]);
                String value = parts[2];
                try {
                    storage.update(id, value);
                    System.out.println("String with id = " + id + " updated");
                } catch (IllegalArgumentException e) {
                    System.err.println(e);
                }
            } else if (input.startsWith(DELETE_COMMAND)) {
                int id = Integer.parseInt(input.substring(DELETE_COMMAND.length() + 1));
                try {
                    storage.delete(id);
                    System.out.println("String with id = " + id + " deleted");
                } catch (IllegalArgumentException e) {
                    System.err.println(e);
                }
            } else if (input.equals(QUIT_COMMAND)) {
                break;
            } else {
                System.out.println("Unknown command");
            }
        }
    }
}
