package ru.khusnullin;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.khusnullin.jsons.JsonSaver;
import ru.khusnullin.models.Person;

import java.io.IOException;

public class Main {
        public static void main (String[] args) {
            //StringSaver.startApp();

            JsonSaver.startApp();

        }
    }