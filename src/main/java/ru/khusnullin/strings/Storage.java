package ru.khusnullin.strings;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

public class Storage {
    private Map<Integer, String> storage = new HashMap<>();
    private int idCounter = 0;
    private String filePath;

    public Storage() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            input = classLoader.getResourceAsStream("config.properties");
            prop.load(input);
            filePath = prop.getProperty("filename");
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        loadFromDisk();
    }

    public int create(String value) {
        Objects.requireNonNull(value, "String value cannot be null");
        int id = ++idCounter;
        storage.put(id, value);
        save();
        return id;
    }

    public String read(int id) {
        String value = storage.get(id);
        if (value == null) {
            throw new IllegalArgumentException("String with id " + id + " does not exist");
        }
        return value;
    }

    public void update(int id, String value) {
        Objects.requireNonNull(value, "String value cannot be null");
        if (!storage.containsKey(id)) {
            throw new IllegalArgumentException("String with id " + id + " does not exist");
        }
        storage.put(id, value);
        save();
    }

    public void delete(int id) {
        if (!storage.containsKey(id)) {
            throw new IllegalArgumentException("String with id " + id + " does not exist");
        }
        storage.remove(id);
        save();
    }

    public Map<Integer, String> getAll() {
        return new HashMap<>(storage);
    }

    private void loadFromDisk() {
        File file = new File(filePath);
        if(!file.exists()) {
            return;
        }
        try(BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while((line = reader.readLine()) != null) {
                String[] parts = line.split(":");
                if(parts.length == 2) {
                    int id = Integer.parseInt(parts[0]);
                    String content = parts[1];
                    storage.put(id, content);
                    idCounter = Math.max(idCounter, id);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void save( ) {
        try(BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for(Map.Entry<Integer, String> entry : storage.entrySet()) {
                writer.write(entry.getKey() + ":" + entry.getValue());
                writer.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}